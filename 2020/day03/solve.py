with open('input.txt', 'r') as f:
    data = f.read()[:-1].split('\n')
        
def count_trees(drop, run):
    row_len, num_trees = len(data[0]), 0
    for i in range((len(data) - 1) // drop + 1):
        num_trees += int(data[drop*i][run*i % row_len] == '#')
    return num_trees
    
print(count_trees(1, 3))
print(count_trees(1, 1) * count_trees(1, 3) * count_trees(1, 5) * count_trees(1, 7) * count_trees(2, 1))

