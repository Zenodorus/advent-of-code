import re

with open('input.txt', 'r') as f:
    data = f.readlines()
    if data[-1] != '\n':
        data.append('\n')
    
passport = {}
num_key_valid = num_value_valid = 0
while data:
    line = data.pop(0)
    if line == '\n':
        for k in passport:
            passport[k] = passport[k].rstrip('\n')
        if set(passport.keys()) >= {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}:
            num_key_valid += 1
            try:
                assert len(passport['byr']) == 4
                assert len(passport['iyr']) == 4
                assert len(passport['eyr']) == 4
                assert 1920 <= int(passport['byr']) <= 2002
                assert 2010 <= int(passport['iyr']) <= 2020
                assert 2020 <= int(passport['eyr']) <= 2030
                if passport['hgt'].endswith('cm'):
                    assert len(passport['hgt']) == 5
                    assert 150 <= int(passport['hgt'][:-2]) <= 193
                elif passport['hgt'].endswith('in'):
                    assert len(passport['hgt']) == 4
                    assert 59 <= int(passport['hgt'][:-2]) <= 76
                else:
                    assert False
                assert passport['hcl'][0] == '#' and re.match(r"[0-9a-fA-F]{6}", passport['hcl'][1:]) is not None
                assert passport['ecl'] in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth')
                assert re.match(r"\d{9}", passport['pid']) is not None
                    
                num_value_valid += 1
            except AssertionError:
                pass
                    
        passport.clear()
    else:
        passport.update([pair.split(':') for pair in line.split(' ')])
print(num_key_valid, num_value_valid)
 
