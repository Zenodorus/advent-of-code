with open('input.txt', 'r') as f:
    data = f.read()
    data = data[:-1].split('\n')
       
def password_check1(line):
    policy, password = line.split(':')
    rng, letter = policy.split(' ')
    low, high = rng.split('-')
    return int(low) <= password.count(letter) <= int(high)
    
print(len(tuple(filter(password_check1, data))))
    
def password_check2(line):
    policy, password = line.split(':')
    pos, letter = policy.split(' ')
    low, high = pos.split('-')
    return (password[int(low)] == letter) != (password[int(high)] == letter)
    
print(len(tuple(filter(password_check2, data))))

