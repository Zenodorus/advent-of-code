with open('input.txt', 'r') as f:
    data = f.read()
    data = list(map(int, data[:-1].split('\n')))
        
for i in range(len(data)):
    for j in range(i+1, len(data)):
        if data[i]+data[j] == 2020:
            print(data[i] * data[j])
                
for i in range(len(data)):
    for j in range(i+1, len(data)):
        for k in range(j+1, len(data)):
            if data[i]+data[j]+data[k] == 2020:
                print(data[i] * data[j] * data[k])

