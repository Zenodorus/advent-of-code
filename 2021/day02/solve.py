with open("input.txt", 'r') as f:
    data = f.readlines()

depth = distance = 0
for line in data:
    inst, v = line.split(' ', 1)
    v = int(v)
    if inst == "forward":
        distance += v
    elif inst == "down":
        depth += v
    elif inst == "up":
        depth -= v

print(depth * distance)

depth = distance = aim = 0
for line in data:
    inst, v = line.split(' ', 1)
    v = int(v)
    if inst == "forward":
        distance += v
        depth += v * aim
    elif inst == "down":
        aim += v
    elif inst == "up":
        aim -= v

print(depth * distance)
