with open("input.txt", 'r') as f:
    data = list(map(int, f.readlines()))

print(sum(data[i] < data[i+1] for i in range(len(data)-1)))
print(sum(data[i] < data[i+3] for i in range(len(data)-3)))
